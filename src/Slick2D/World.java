package Slick2D;

/**
 * @autor Nyrkov Ivan
 * Date: 09.04.14
 * Time: 12:30
 * World
 */
public class World {

    public static float offsetX = 0;
    public static float offsetY = 0;

    public static float tileHeight = 32;
    public static float tileWidth = 64;

}
