package Slick2D;

import org.newdawn.slick.*;
import org.newdawn.slick.tiled.TiledMap;

import static Slick2D.World.*;

/**
 * @autor Nyrkov Ivan
 * Date: 09.04.14
 * Time: 11:24
 * Game
 */
public class Game extends BasicGame {

    private TiledMap testMap;
    private Image player;
    private boolean[][] blocked;
    private float x = 128, y = 128;

    public Game() {
        super("Test Game");
    }

    public static void main(String[] arguments) {
        try {
            AppGameContainer app = new AppGameContainer(new Game());
            //app.setDisplayMode(900, 600, false);
            app.setFullscreen(true);
            app.setTargetFrameRate(60);
            app.start();
        }
        catch (SlickException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        testMap = new TiledMap("res/testmap.tmx");
        player = new Image("res/images/Mage2.png");

        blocked = new boolean[testMap.getWidth()][testMap.getHeight()];
        for (int x = 0; x < testMap.getWidth(); x++) {
            for (int y = 0; y < testMap.getHeight(); y++) {
                int tileID = testMap.getTileId(x, y, 0);
                String value = testMap.getTileProperty(tileID, "blocked", "false");
                if ("true".equals(value)) {
                    blocked[x][y] = true;
                }
            }
        }
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        Input input = gameContainer.getInput();
        if (input.isKeyDown(Input.KEY_UP)) {
            if (!isBlocked(x, y - delta * 0.1f)) {
                //player.update(delta);
                // The lower the delta the slowest the sprite will animate.
                y -= delta * 0.1f;
            }
        }
        else if (input.isKeyDown(Input.KEY_DOWN)) {
            //sprite = down;
            if (!isBlocked(x, y + 32 + delta * 0.1f)) {
              //  sprite.update(delta);
                y += delta * 0.1f;
            }
        }
        else if (input.isKeyDown(Input.KEY_LEFT)) {
            //sprite = left;
            if (!isBlocked(x - delta * 0.1f, y)) {
              //  sprite.update(delta);
                x -= delta * 0.1f;
            }
        }
        else if (input.isKeyDown(Input.KEY_RIGHT)) {
            //sprite = right;
            if (!isBlocked(x + 64 + delta * 0.1f, y)) {
              //  sprite.update(delta);
                x += delta * 0.1f;
            }
        }
        else if (input.isKeyDown(Input.KEY_ESCAPE)) {
            System.exit(0);
        }
    }


    private boolean isBlocked(float x, float y) {
        int xBlock = (int)x / 62;
        int yBlock = (int)y / 34;

        return blocked[xBlock][yBlock];
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        testMap.render((int) (x+offsetX), (int) (y+offsetY));
        player.draw(x, y, 64, 64);
    }
}
