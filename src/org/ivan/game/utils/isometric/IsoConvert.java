package org.ivan.game.utils.isometric;


/**
 * @autor Nyrkov Ivan
 * Date: 11.04.14
 * Time: 21:43
 * IsoConvert
 */
public abstract class IsoConvert {

    public static Float getColumn(Float x, Float y, int width, int height) {
        return (float) Math.round(((y - height / 2) / height) + ((x - width / 2) / width));
    }
    public static Float getRow(Float x, Float y, int width, int height) {
        return (float) Math.round(((y - height / 2) / height) - ((x - width / 2) / width));
    }

    /*public static Float getX(Float i, Float j, int tileWidth, Boolean isTileCell) {
        Float x = ((i - j) * (tileWidth / 2));
        if (isTileCell) {
              x += World.TILE_WIDTH / 2 - World.TILE_CELL_WIDTH / 2;
        }
        return x;
    }
    public static Float getY(Float i, Float j, int tileHeight, Boolean isTileCell) {
        Float y = ((i + j) * (tileHeight / 2));
        return y;
    }  */
}
