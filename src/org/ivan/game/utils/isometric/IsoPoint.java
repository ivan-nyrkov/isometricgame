package org.ivan.game.utils.isometric;


import org.ivan.game.World;

/**
 * @autor Nyrkov Ivan
 * Date: 11.04.14
 * Time: 11:04
 * IsoPoint
 */
public class IsoPoint {

   /* private Float x;
    private Float y;
    private Integer i;
    private Integer j;
    private Boolean isTileCell = false;

    public IsoPoint(Float x, Float y) {
        this.x = x;
        this.y = y;
        initIsoIndx();
    }

    public IsoPoint(Integer i, Integer j) {
        this.i = i;
        this.j = j;
        initIsoCoords();
    }

    public IsoPoint(Float x, Float y, Boolean isTileCell) {
        this.x = x;
        this.y = y;
        initIsoIndx();
        this.isTileCell = isTileCell;
    }

    public IsoPoint(Integer i, Integer j, Boolean isTileCell) {
        this.i = i;
        this.j = j;
        initIsoCoords();
        this.isTileCell = isTileCell;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IsoPoint)) return false;

        IsoPoint point = (IsoPoint) o;

        if (i != null ? !i.equals(point.i) : point.i != null) return false;
        if (j != null ? !j.equals(point.j) : point.j != null) return false;
        if (x != null ? !x.equals(point.x) : point.x != null) return false;
        if (y != null ? !y.equals(point.y) : point.y != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x != null ? x.hashCode() : 0;
        result = 31 * result + (y != null ? y.hashCode() : 0);
        result = 31 * result + (i != null ? i.hashCode() : 0);
        result = 31 * result + (j != null ? j.hashCode() : 0);
        return result;
    }

    public Float getY() {
        if (y == null) {
            initIsoCoords();
        }
        return y;
    }

    public void setY(Float y) {
        this.y = y;
        this.j = null;
    }

    public Float getX() {
        if (x == null) {
            initIsoCoords();
        }
        return x;
    }

    public void setX(Float x) {
        this.x = x;
        this.i = null;
    }

    public Integer getI() {
        if (i == null) {
            initIsoIndx();
        }
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
        this.x = null;
    }

    public Integer getJ() {
        if (j == null) {
            initIsoIndx();
        }
        return j;
    }

    public void setJ(Integer j) {
        this.j = j;
        this.y= null;
    }

    private void initIsoIndx() {
        Integer tileHeight;
        Integer tileWidth;
        if (isTileCell) {
            tileHeight = World.TILE_CELL_HEIGHT;
            tileWidth = World.TILE_CELL_WIDTH;
        } else {
            tileHeight = World.TILE_HEIGHT;
            tileWidth = World.TILE_WIDTH;
        }
        Integer indxI = Math.round(((y - tileHeight / 2) / tileHeight) + ((x - tileWidth / 2) / tileWidth));
        Integer indxJ = Math.round(((y - tileHeight / 2) / tileHeight) - ((x - tileWidth / 2) / tileWidth));
        if (isTileCell) {
            indxI -= 2;
            indxJ += 2;
        }
        this.i = indxI;
        this.j = indxJ;
    }

    private void initIsoCoords() {
        Integer tileWidth;
        Integer tileHeight;
        if (isTileCell) {
            tileHeight = World.TILE_CELL_HEIGHT;
            tileWidth = World.TILE_CELL_WIDTH;
        } else {
            tileHeight = World.TILE_HEIGHT;
            tileWidth = World.TILE_WIDTH;
        }
        Float x = ((i - j) * ((float)tileWidth / 2));
        Float y = ((i + j) * ((float)tileHeight / 2));
        if (isTileCell) {
            x += World.TILE_WIDTH / 2 - World.TILE_CELL_WIDTH / 2;
        }
        this.x = x;
        this.y = y;
    }     */

}
