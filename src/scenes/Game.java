package scenes;

import org.newdawn.slick.*;


public class Game extends BasicGame 
{	
	public static SceneManager manager;
	
	// Render verything
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException 
	{
		manager.render(gc, g);
	}

	// Do our logic 
	@Override
	public void update(GameContainer gc, int t) throws SlickException 
	{
		manager.update(gc, t);
	}
	
	// Init our Gameobjects
	@Override
	public void init(GameContainer gc) throws SlickException 
	{
		manager = new SceneManager(gc);
		manager.addSence( new Scene1 () );
	}
	
	public Game ()
	{
		super("Game");
	}
}
