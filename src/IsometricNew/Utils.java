package IsometricNew;

import org.lwjgl.Sys;

/**
 * @autor Nyrkov Ivan
 * Date: 08.04.14
 * Time: 15:58
 * TilesUtils
 */
public class Utils {

    public static long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    public static int toIsoX(int x, int y) {
        return ((x - y) * (World.BLOCK_WIDTH / 2));
    }
    public static int toIsoY(int x, int y) {
        return ((x + y) * (World.BLOCK_HEIGHT / 2));
    }

    public static double toIsoX(double x, double y) {
        return ((x - y) * (World.BLOCK_WIDTH / 2));
    }
    public static double toIsoY(double x, double y) {
        return ((x + y) * (World.BLOCK_HEIGHT / 2));
    }

    public static int fromIsoX(int x, int y) {
        return ((x / (World.BLOCK_WIDTH / 2) + y / (World.BLOCK_HEIGHT / 2)) / 2);
    }
    public static double fromIsoX(double x, double y) {
        return ((x / (World.BLOCK_WIDTH / 2) + y / (World.BLOCK_HEIGHT / 2)) / 2);
    }

    public static int fromIsoY(int x, int y) {
        return ((y / (World.BLOCK_HEIGHT / 2) - (x / (World.BLOCK_WIDTH / 2))) / 2);
    }
    public static double fromIsoY(double x, double y) {
        return ((y / (World.BLOCK_HEIGHT / 2) - (x / (World.BLOCK_WIDTH / 2))) / 2);
    }
}
