/*
 * Copyright (c) 2013, Oskar Veerhoek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package IsometricNew;


import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;

public class Boot {

    private BlockGrid grid;
    private int selector_x = 0;
    private int selector_y = 0;
    private boolean mouseEnabled = true;
    private Player player;

    public Boot() {
        try {
            //Display.setDisplayMode(new DisplayMode(640, 480));
            Display.setFullscreen(true);
            Display.setTitle("Isometric");
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }

        World.offsetX = Display.getWidth() / 2;
        System.out.println(World.offsetX);
        World.offsetY = Display.getHeight() / 2;
        System.out.println(World.offsetY);
        grid = new BlockGrid();
        player = new Player(4, 4);

        // Initialization code OpenGL
        initGl();

        while (!Display.isCloseRequested()) {
            // Render
            input();

            draw();

            Display.update();
            Display.sync(60);
        }
        Display.destroy();
        System.exit(0);
    }

    private void initGl() {
        glMatrixMode(GL_PROJECTION);        // 2d
        glLoadIdentity();                   // сброс матрицы


        glOrtho(0, 1280, 800, 0, 1, -1);     // cистема координат
        glMatrixMode(GL_MODELVIEW);         // управление видом

        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    private void input() {
        if (mouseEnabled || Mouse.isButtonDown(0)) {
            mouseEnabled = true;
            boolean mouseClicked = Mouse.isButtonDown(0);

            double mouseX = (double) Mouse.getX() - World.offsetX;
            double mouseY = (double) Display.getHeight() - Mouse.getY() - 1 - World.offsetY;

            selector_x = (int) Utils.fromIsoX(mouseX, mouseY);
            selector_y = (int) Math.ceil(Utils.fromIsoY(mouseX, mouseY));

            System.out.println(selector_x + ":" + selector_y);
        }

        while (Keyboard.next()) {
            if (Keyboard.getEventKey() == Keyboard.KEY_RIGHT && Keyboard.getEventKeyState()) {
                mouseEnabled = false;
                goRight();
            }
            if (Keyboard.getEventKey() == Keyboard.KEY_LEFT && Keyboard.getEventKeyState()) {
                mouseEnabled = false;
                goLeft();
            }
            if (Keyboard.getEventKey() == Keyboard.KEY_UP && Keyboard.getEventKeyState()) {
                mouseEnabled = false;
                goUp();
            }
            if (Keyboard.getEventKey() == Keyboard.KEY_DOWN && Keyboard.getEventKeyState()) {
                mouseEnabled = false;
                goDown();
            }
            if (Keyboard.getEventKey() == Keyboard.KEY_C) {
                grid.clear();
            }
            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                Display.destroy();
                System.exit(0);
            }
        }
    }

    private void draw() {
        glClear(GL_COLOR_BUFFER_BIT);
        grid.draw();
        drawSelectionBox();
        player.draw();
    }

    private void drawSelectionBox() {
        int x = Utils.toIsoX(selector_x, selector_y) + World.offsetX;
        int y = Utils.toIsoY(selector_x, selector_y) + World.offsetY;

        if (((selector_x < 0) || (selector_y < 0) ||
                (selector_x >= World.BLOCKS_WIDTH)) || (selector_y >= World.BLOCKS_HEIGHT)) {
            return;
        }

       /* if (grid.getAt(selector_x, selector_y).getType() != BlockType.AIR) {
            glBindTexture(GL_TEXTURE_2D, 0);
            glColor4f(1f, 1f, 1f, 0.5f);
            glBegin(GL_QUADS);
                glVertex2i(x, y);
                glVertex2i(x + World.BLOCK_WIDTH, y);
                glVertex2i(x + World.BLOCK_WIDTH, y + World.BLOCK_HEIGHT);
                glVertex2i(x, y + World.BLOCK_HEIGHT);
            glEnd();
            glColor4f(1f, 1f, 1f, 1f);
        } else {*/
            glColor4f(1f, 1f, 1f, 0.5f);
            new Block(BlockType.STONE , selector_x, selector_y).draw();
            glColor4f(1f, 1f, 1f, 1f);
        //}
    }

    private void goUp() {
        if (!(selector_y - 1 < 0)) {
            selector_y -= 1;
        }
        if (!(selector_x - 1 < 0)) {
            selector_x -= 1;
        }
    }

    private void goDown() {
        if (!(selector_x + 1 >= World.BLOCKS_WIDTH)) {
            selector_x += 1;
        }
        if (!(selector_y + 1 >= World.BLOCKS_HEIGHT)) {
            selector_y += 1;
        }
    }

    private void goLeft() {
        if (!(selector_x - 1 < 0)) {
            selector_x -= 1;
        }
        if (!(selector_y + 1 >= World.BLOCKS_HEIGHT)) {
            selector_y += 1;
        }
    }

    private void goRight() {
        if (!(selector_x + 1 >= World.BLOCKS_WIDTH)) {
            selector_x += 1;
        }
        if (!(selector_y - 1 < 0)) {
            selector_y -= 1;
        }
    }

    public static void main(String[] args) {
        new Boot();
    }
}
