package IsometricNew;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glPopMatrix;

/**
 * @autor Nyrkov Ivan
 * Date: 08.04.14
 * Time: 16:27
 * Player
 */
public class Player {

    private Texture texture;
    private String textureLocation = "res/images/Mage2.png";
    private float x;   // isometric x
    private float y;   // isometric y


    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void move(int x, int y) {
        this.x = Utils.toIsoX(x, y) + World.offsetX + (World.BLOCK_WIDTH / 2 - World.PLAYER_WIDTH / 2);
        this.y = Utils.toIsoY(x, y) + World.offsetY + (World.BLOCK_HEIGHT / 2) - World.PLAYER_HEIGHT  + (World.BLOCK_HEIGHT / 10);;
    }

    public Player(int x, int y) {
        this.x = Utils.toIsoX(x, y) + World.offsetX + (World.BLOCK_WIDTH / 2 - World.PLAYER_WIDTH / 2);
        this.y = Utils.toIsoY(x, y) + World.offsetY + (World.BLOCK_HEIGHT / 2) - World.PLAYER_HEIGHT + (World.BLOCK_HEIGHT / 10);

        // - (World.PLAYER_HEIGHT - World.BLOCK_HEIGHT );
        try {
            this.texture = TextureLoader.getTexture("PNG", new FileInputStream(new File(textureLocation)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void draw() {
        glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
        glPushMatrix();
        glTranslatef(x, y, 0);
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2f(0, 0);
            glTexCoord2f(1, 0);
            glVertex2f(World.PLAYER_WIDTH, 0);
            glTexCoord2f(1, 1);
            glVertex2f(World.PLAYER_WIDTH, World.PLAYER_HEIGHT);
            glTexCoord2f(0, 1);
            glVertex2f(0, World.PLAYER_HEIGHT);
        glEnd();
        glPopMatrix();
    }

}